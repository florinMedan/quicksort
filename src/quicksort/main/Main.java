package quicksort.main;

import quicksort.parallel.ParallelQuick;
import quicksort.sequential.SeqQuick;
import quicksort.utils.Utils;

import java.io.IOException;


public class Main {
    static final int MAX = 200000000;

    public static void main(String[] args) {
        Utils utils=new Utils();
        int[] sortArrayS = new int[MAX],sortArrayP = new int[MAX];

        try {
            //utils.writeRandomNumbers(MAX);
            utils.readFromFile(sortArrayS,"quicksortin.txt",MAX);
            //utils.readFromFile(sortArrayP,"quicksortin.txt",MAX);
            SeqQuick seqQuick = new SeqQuick();
            long startTime = System.currentTimeMillis();
            seqQuick.sort(sortArrayS, 0, sortArrayS.length - 1);
            long stopTime=System.currentTimeMillis();
            boolean verificareOrdonare= utils.verificareOronare(sortArrayS);
            utils.writeToFile(sortArrayS,"quicksortout", stopTime- startTime,1,verificareOrdonare);


            /*ParallelQuick parallelQuick = new ParallelQuick();
            parallelQuick.sort(sortArrayP);
            writeToFile(sortArrayS,"quicksortout",System.currentTimeMillis() - startTime,2);*/
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
