package quicksort.sequential;


public class SeqQuick {

    private int partition(int[] arrayToSort, int low, int high) {
        int pivot = arrayToSort[high];
        int i = (low - 1);
        for (int j = low; j < high; j++) {
            if (arrayToSort[j] < pivot) {
                i++;
                swap(arrayToSort, i, j);
            }
        }
        swap(arrayToSort, i + 1, high);

        return i + 1;
    }

    private void swap(int[] arrayToSort, int i, int j) {
        int temp = arrayToSort[i];
        arrayToSort[i] = arrayToSort[j];
        arrayToSort[j] = temp;
    }


    public void sort(int[] arrayToSort, int low, int high) {
        if (low < high) {
            int pi = partition(arrayToSort, low, high);

            sort(arrayToSort, low, pi - 1);
            sort(arrayToSort, pi + 1, high);
        }
    }
}
