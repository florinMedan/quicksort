package quicksort.parallel;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ParallelQuick {

    public void sort(int[] sortArray) {
                final ExecutorService executor = Executors.newFixedThreadPool(10);
        List futures = new Vector();
        QSTask rootTask = new QSTask(executor, futures, sortArray, 0, sortArray.length - 1);
        long startTime = System.currentTimeMillis();
        futures.add(executor.submit(rootTask));
        while (!futures.isEmpty()) {
            Future topFeature = (Future) futures.remove(0);
            try {
                if (topFeature != null) topFeature.get();
            } catch (InterruptedException | ExecutionException ie) {
                ie.printStackTrace();
            }
        }
        executor.shutdown();
    }
}