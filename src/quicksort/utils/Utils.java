package quicksort.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Utils {

    public boolean verificareOronare(int[] array){
        for(int i=1;i<array.length;i++)
            if(array[i-1]>array[i])
                return false;
        return true;
    }

    public  void writeRandomNumbers(int maxLength) throws IOException {
        Random random = new Random();

        int[] array = new int[maxLength];
        for (int i = 0; i < maxLength; i++) {
            array[i] = random.nextInt(maxLength);
        }
        writeToFile(array,"quicksortin.txt",0,0,true);

    }

    public void writeToFile(int[] sortArray, String fileName,long time, int ups,boolean verificare) throws IOException {
        if(ups==0) {
            int k=-1,kp=0;
            FileWriter myWriter = new FileWriter(fileName);
            for (int i : sortArray) {
                if(kp%5==0)
                    myWriter.write(i*k+ " ");
                else
                    myWriter.write(i+ " ");
                kp++;
                if(kp%100000==0)
                    myWriter.write("\n");
            }
            myWriter.close();
            return;
        }
        if(ups==1) {
            FileWriter myWriter = new FileWriter(fileName+"S.txt");
            if(verificare) {
                myWriter.write("Sirul este ordonat corespunzator\n");
                myWriter.write("Sequential time taken: " + time  + " milisecondes\n");
                /*int k=0;
                for(int i:sortArray){
                    myWriter.write(i+ " ");
                    k++;
                    if(k%100000==0)
                        myWriter.write("\n");
                }
                myWriter.write("Sequential sort result: " + Arrays.toString(sortArray));*/
            }
            else
                myWriter.write("Sirul nu este ordonat corespunzator\n");
            myWriter.close();
        }
        else{
            FileWriter myWriter = new FileWriter(fileName+"P.txt");
            if(verificare) {
                myWriter.write("Sirul este ordonat corespunzator\n");
                myWriter.write("Parallel time taken: " + time / 60 + " seconds\n");
                //myWriter.write("Parallel sort result: " + Arrays.toString(sortArray));
            }
            else
                myWriter.write("Sirul nu este ordonat corespunzator\n");
            myWriter.close();
        }
    }

    public void readFromFile(int[] sortArray, String filename,int maxLegth) throws FileNotFoundException {
        Scanner s = new Scanner(new File(filename));
        String[] line;
        int i = 0;
        int k;
        if(maxLegth>100000)
            k=maxLegth/100000;
        else
            k=1;
        line = s.nextLine().split(" ");
        while(k!=0) {
            for (String nr : line)
                if (i < maxLegth)
                    sortArray[i++] = Integer.parseInt(nr);
                else
                    break;
            k--;
            if(k!=0)
                line = s.nextLine().split(" ");
        }
    }
}
